import UserState from "@/store/modules/user/types";
import {ActionTree, GetterTree, MutationTree} from "vuex";
import {RootState} from "@/store/types";
import IUser from "@/domain/entities/interfaces/IUser";
import Api from "@/domain/services/Api";
import User from "@/domain/entities/User";

export const state: UserState = {
    profile: new User(undefined, undefined, undefined),
    role: undefined,
    error: false,
}

export const getters: GetterTree<UserState, RootState> = {
    hasRole: state => (role: string) => {
        return role == state.role;
    },
}

export const actions: ActionTree<UserState, RootState> = {

    getUser({commit}) {
        Api.user().then((response: any) => {
            const playload: IUser = response;
            commit('userLoaded', playload);
        }).catch(error => {
            commit('userError');
        })
    },

    removeUser({commit}) {
        commit('userClear');
    },
}

export const mutations: MutationTree<UserState> = {

    userLoaded(state, payload: IUser) {
        state.profile = payload;
        state.role = payload.role.title;
        state.error = false;
    },

    userError(state) {
        state.profile = new User(undefined, undefined, undefined);
        state.role = undefined;
        state.error = true;
    },

    userClear(state) {
        state.profile = new User(undefined, undefined, undefined);
        state.role = undefined;
        state.error = false;
    },

}

export const user = {
    state,
    getters,
    actions,
    mutations,
}
