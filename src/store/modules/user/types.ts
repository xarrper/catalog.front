import IUser from "@/domain/entities/interfaces/IUser";

interface UserState {
    profile: IUser;
    role?: string;
    error: boolean;
}

export default UserState;
