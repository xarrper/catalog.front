import AuthState from "@/store/modules/auth/types";
import {ActionTree, GetterTree, MutationTree} from "vuex";
import {RootState} from "@/store/types";
import Api from "@/domain/services/Api";
import IDataAuth from "@/domain/entities/interfaces/IDataAuth";
import TokenService from "@/domain/services/TokenService";

export const state: AuthState = {
    token: TokenService.getToken(),
    error: false,
}

export const getters: GetterTree<AuthState, RootState> = {
    isAuthenticated: state => !!state.token,
}

export const actions: ActionTree<AuthState, RootState> = {

    authLogin({commit, dispatch}, payload: IDataAuth) {

        return new Promise((resolve, reject) => {
            Api.login(payload).then((response: any) => {
                const playload: string = response.token;
                TokenService.setToken(playload);
                commit('authLoaded');
                dispatch('getUser');
                resolve();
            }).catch(error => {
                TokenService.destroy();
                commit('authError');
                reject(error.response.status);
            });
        });

    },

    authLogout({commit, dispatch}) {
        return Api.logout().then(response => {
            TokenService.destroy();
            commit('authLoaded');
            dispatch('removeUser');
        }).catch(error => {
            TokenService.destroy();
            commit('authError');
            dispatch('removeUser');
        });
    },

}

export const mutations: MutationTree<AuthState> = {

    authLoaded(state) {
        state.token = TokenService.getToken();
        state.error = false;
    },

    authError(state) {
        state.token = TokenService.getToken();
        state.error = true;
    },

}

export const auth = {
    state,
    getters,
    actions,
    mutations,
}
