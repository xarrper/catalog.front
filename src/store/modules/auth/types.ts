interface AuthState {
    token: string;
    error: boolean;
}

export default AuthState;
