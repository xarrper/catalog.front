import IProduct from "@/domain/entities/interfaces/IProduct";

interface ProductState {
    product: IProduct;
    products: IProduct[];
    error: boolean;
}

export default ProductState;
