import ProductState from "@/store/modules/product/types";
import {ActionTree, MutationTree} from "vuex";
import {RootState} from "@/store/types";
import IProduct from "@/domain/entities/interfaces/IProduct";
import Api from "@/domain/services/Api";
import IFilter from "@/domain/entities/interfaces/IFilter";
import Product from "@/domain/entities/Product";
import AppService from "@/domain/services/AppService";

export const state: ProductState = {
    products: [new Product(undefined, undefined, undefined, undefined, undefined, undefined)],
    product: new Product(undefined, undefined, undefined, undefined, undefined, undefined),
    error: false,
};

export const actions: ActionTree<ProductState, RootState> = {

    allProducts({commit}, playload: IFilter) {
        Api.products(playload).then((response: any) => {
            const playload: IProduct[] = response;
            commit('productsLoaded', playload);
        }).catch(error => {
            commit('productsError');
        })
    },

    getProduct({commit}, playload: { id: number }) {
        Api.product(playload.id).then((response: any) => {
            const playload: IProduct = response;
            commit('productLoaded', playload);
        }).catch(error => {
            commit('productError');
        })
    },

    destroyProduct({commit, dispatch}, playload: { id: number, filter: IFilter }) {
        return new Promise((resolve, reject) => {
            Api.delete(playload.id).then((response: any) => {
                dispatch('allProducts', playload.filter);
                resolve(response);
            }).catch(error => {
                reject(error);
            })
        });
    },

    updateProduct() {
        return new Promise((resolve, reject) => {
            Api.update(state.product).then((response: any) => {
                resolve(response.message);
            }).catch((error) => {
                const message = AppService.getErrorMessage(error.response.data.errors);
                reject(message);
            });
        });
    },

    saveProduct({commit}) {
        return new Promise((resolve, reject) => {
            Api.store(state.product).then((response: any) => {
                commit('productSave');
                resolve(response.message);
            }).catch((error) => {
                const message = AppService.getErrorMessage(error.response.data.errors);
                reject(message);
            });
        });
    }

}

export const mutations: MutationTree<ProductState> = {

    productsLoaded(state, payload: IProduct[]) {
        state.products = payload;
        state.error = false;
    },

    productsError(state) {
        state.products = [new Product(undefined, undefined, undefined, undefined, undefined, undefined)];
        state.error = true;
    },

    productLoaded(state, payload: IProduct) {
        state.product = payload;
        state.error = false;
    },

    productError(state) {
        state.product = new Product(undefined, undefined, undefined, undefined, undefined, undefined);
        state.error = true;
    },

    productSave(state) {
        state.product = new Product(undefined, undefined, undefined, undefined, undefined, undefined);
        state.error = false;
    }

}

export const product = {
    state,
    actions,
    mutations,
}
