import CategoryState from "@/store/modules/category/types";
import Category from "@/domain/entities/Category";
import {ActionTree, Module, MutationTree} from "vuex";
import {RootState} from "@/store/types";
import Api from "@/domain/services/Api";

export const state: CategoryState = {
    categories: [new Category(undefined, undefined)],
    error: false,
};

export const actions: ActionTree<CategoryState, RootState> = {

    allCategories({commit}) {
        Api.categories().then((response: any) => {
            const playload: Category[] = response;
            commit('categoriesLoaded', playload);
        }).catch(error => {
            commit('categoriesError');
        })
    },

}

export const mutations: MutationTree<CategoryState> = {

    categoriesLoaded(state, payload: Category[]) {
        state.categories = payload;
        state.error = false;
    },

    categoriesError(state) {
        state.categories = [new Category(undefined, undefined)];
        state.error = true;
    }

}

export const category: Module<CategoryState, RootState> = {
    state,
    actions,
    mutations,
}
