import ICategory from "@/domain/entities/interfaces/ICategory";

interface CategoryState {
    categories: ICategory[];
    error: boolean;
}

export default CategoryState;
