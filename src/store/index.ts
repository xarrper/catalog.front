import Vue from 'vue';
import Vuex from 'vuex';
import {auth} from '@/store/modules/auth/auth';
import {user} from '@/store/modules/user/user';
import {product} from '@/store/modules/product/product';
import {category} from '@/store/modules/category/category';
import {RootState} from "@/store/types";

Vue.use(Vuex);

export default new Vuex.Store<RootState>({

    state: {
        applicationName: 'catalog',
    },

    modules: {
        auth,
        user,
        product,
        category,
    },

})
