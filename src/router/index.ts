import Vue from 'vue'
import Router from 'vue-router'
import List from '@/views/products/List.vue';
import Show from '@/views/products/Show.vue';
import Login from '@/views/auth/Login.vue';
import Forbidden from '@/views/errors/Forbidden.vue';
import NotFound from '@/views/errors/NotFound.vue';
import {ifNotAuthenticated, ifAuthenticated} from "@/domain/util/middleware/Authenticated";
import {ADMIN, USER, role} from "@/domain/util/middleware/RoleChecker";
import Edit from "@/views/products/Edit.vue";
import Create from "@/views/products/Create.vue";

Vue.use(Router);

const index = new Router({
    mode: 'history',
    routes: [

        {
            path: '/', name: 'products.list', component: List,
        },
        {
            path: '/products/create', name: 'products.create', props: true, component: Create,
            beforeEnter: (to, from, next) => {
                if(ifAuthenticated(to, from, next)) {
                    role(next, ADMIN);
                }
            },
        },
        {
            path: '/products/:id', name: 'products.show', component: Show, props: true,
        },
        {
            path: '/products/:id/edit', name: 'products.edit', props: true, component: Edit,
            beforeEnter: (to, from, next) => {
                if(ifAuthenticated(to, from, next)) {
                    role(next, ADMIN);
                }
            },
        },
        {
            path: '/login', name: 'login', component: Login, beforeEnter: ifNotAuthenticated,
        },

        {path: '/forbidden', component: Forbidden},
        {path: '*', component: NotFound},
    ]
});

export default index;



