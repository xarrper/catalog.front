import ApiService from '@/domain/services/ApiService';
import IFilter from "@/domain/entities/interfaces/IFilter";
import IDataAuth from "@/domain/entities/interfaces/IDataAuth";
import IProduct from "@/domain/entities/interfaces/IProduct";

class Api {

    static categories() {
        return ApiService.get('/categories');
    }

    static product(id: number) {
        return ApiService.get('/products/' + id);
    }

    static products(filter: IFilter) {
        return ApiService.get('/products', filter);
    }

    static user() {
        return ApiService.get('/getUser');
    }

    static login(dataAuth: IDataAuth) {
        return ApiService.post('/login', dataAuth);
    }

    static logout() {
        return ApiService.post('/logout');
    }

    static store(product: IProduct) {
        return ApiService.post('/products', product);
    }

    static update(product: IProduct) {
        return ApiService.put('/products/' + product.id, product);
    }

    static delete(id: number) {
        return ApiService.delete('/products/' + id);
    }

}

export default Api;
