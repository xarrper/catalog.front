import axios from 'axios';
import ErrorNotifier from "@/domain/util/notifications/ErrorNotifier";

class ApiService {

    private static url: string = process.env.VUE_APP_BACK_URL;

    static post(url: string, data: any = {}) {
        return this.any('POST', url, data, {});
    }

    static put(url: string, data: any = {}) {
        return this.any('PUT', url, data, {});
    }

    static get(url: string, params: any = {}) {
        return this.any('GET', url, {}, params);
    }

    static delete(url: string) {
        return this.any('DELETE', url, {}, {});
    }

    private static any(type: string, url: string, data: any, params: any) {
        return new Promise((resolve, reject) => {
            axios({
                method: type,
                url: this.url + url,
                data: data,
                params: params,
            }).then(response => {
                this.success();
                resolve(response.data);
            }).catch(error => {
                this.fail();
                reject(error);
            });
        });
    }

    private static fail() {
        ErrorNotifier.notify();
    }

    private static success() {
    }
}

export default ApiService;
