import axios from "axios";

class TokenService {

    private static prefix: string = 'Bearer ';

    public static getToken() {
        const userToken = localStorage.getItem('user-token') || '';
        axios.defaults.headers.common['Authorization'] = userToken;
        return userToken;
    }

    public static setToken(token: string) {
        const userToken =  this.prefix + token;
        localStorage.setItem('user-token', userToken);
        axios.defaults.headers.common['Authorization'] = userToken;
    }

    public static destroy() {
        localStorage.removeItem('user-token');
        delete axios.defaults.headers.common['Authorization'];
    }

}

export default TokenService;
