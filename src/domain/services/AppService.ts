class AppService {

    public static getErrorMessage(errors: any) {

        let message = '';

        Object.keys(errors).map((objectKey, index) => {
            errors[objectKey].forEach((value: any, key: any)=> {
                message += value + '<br>';
            });
        });

        return message;
    }

}

export default AppService;
