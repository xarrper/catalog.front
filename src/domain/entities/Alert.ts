import IAlert from "@/domain/entities/interfaces/IAlert";

class Alert implements IAlert {

    public show: boolean;
    public type: string;
    public message: string;

    public static SUCCESS: string = 'success';
    public static ERROR: string = 'danger';

    constructor(show: boolean = false, type: string = '', message: string = '') {
        this.show = show;
        this.type = type;
        this.message = message;
    }

}

export default Alert;
