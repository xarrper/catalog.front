import ICategory from "@/domain/entities/interfaces/ICategory";

class Category implements ICategory {
    public id?: number;
    public title?: string;

    constructor(id?: number, title?: string) {
        this.id = id;
        this.title = title;
    }
}

export default Category;
