import IFilter from "@/domain/entities/interfaces/IFilter";

class Filter implements IFilter {

    public search?: string;
    public category?: number;
    public costFrom?: string;
    public costTo?: string;

    constructor(search?: string, category?: number, costFrom?: string, costTo?: string) {
        this.search = search;
        this.category = category;
        this.costFrom = costFrom;
        this.costTo = costTo;
    }

}

export default Filter;
