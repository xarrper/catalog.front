import IUser from "@/domain/entities/interfaces/IUser";
import IRole from "@/domain/entities/interfaces/IRole";
import Role from "@/domain/entities/Role";

class User implements IUser{
    public id?: number;
    public username?: string;
    public role_id?: number;
    public role: IRole;

    constructor(id?: number, username?: string, role_id?: number, role: IRole = new Role(undefined, undefined)) {
        this.id = id;
        this.username = username;
        this.role_id = role_id;
        this.role = role;
    }
}

export default User;
