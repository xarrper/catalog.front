import IRole from "@/domain/entities/interfaces/IRole";

class Role implements IRole {
    public id?: number;
    public title?: string;

    constructor(id?: number, title?: string) {
        this.id = id;
        this.title = title;
    }
}

export default Role;
