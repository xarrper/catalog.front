interface IRole {
    id?: number;
    title?: string;
}

export default IRole;
