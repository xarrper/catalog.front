interface IFilter {
    search?: string;
    category?: number;
    costFrom?: string;
    costTo?: string;
}

export default IFilter;
