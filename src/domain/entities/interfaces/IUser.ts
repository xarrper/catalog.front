import IRole from "@/domain/entities/interfaces/IRole";

interface IUser {
    id?: number;
    username?: string;
    role_id?: number;
    role: IRole;
}

export default IUser;
