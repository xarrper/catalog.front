interface IDataAuth {
    username?: string;
    password?: string;
}

export default IDataAuth;
