import ICategory from "@/domain/entities/interfaces/ICategory";

interface IProduct {
    id?: number;
    title?: string;
    cost?: number;
    description?: string;
    category_id?: number;
    user_id?: number;
    category: ICategory;
}

export default IProduct;
