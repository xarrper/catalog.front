interface IAlert {
    show: boolean;
    type: string;
    message: string;
}

export default IAlert;
