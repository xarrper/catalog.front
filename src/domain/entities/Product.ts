import IProduct from "@/domain/entities/interfaces/IProduct";
import ICategory from "@/domain/entities/interfaces/ICategory";
import Category from "@/domain/entities/Category";

class Product implements IProduct {

    public id?: number;
    public title?: string;
    public cost?: number;
    public description?: string;
    public category_id?: number;
    public user_id?: number;
    public category: ICategory;

    constructor(id?: number, title?: string, cost?: number, description?: string, category_id?: number, user_id?: number, category: ICategory = new Category(undefined, undefined)) {
        this.id = id;
        this.title = title;
        this.cost = cost;
        this.description = description;
        this.category_id = category_id;
        this.user_id = user_id;
        this.category = category;
    }

}

export default Product;
