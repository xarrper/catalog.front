import axios from 'axios';

const ADMIN = 'admin';
const USER = 'user';

// TODO: получать через store
function login(){
    return axios.get(process.env.VUE_APP_BACK_URL + '/getUser');
}

const role = (next: any, role: any) => {

    login().then((response) => {
        if (response.data.role.title === role){
            next()
        } else {
            next('/forbidden');
        }
    }).catch((error) => {
        next('/forbidden');
    });

}

export {ADMIN, USER, role};
