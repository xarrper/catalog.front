import TokenService from "@/domain/services/TokenService";

const ifNotAuthenticated = (to: any, from: any, next: any) => {
    if (!!!TokenService.getToken()) {
        next();
        return true;
    }
    next('/');
    return false;
}

const ifAuthenticated = (to: any, from: any, next: any) => {
    if (!!TokenService.getToken()) {
        next();
        return true;
    }
    next('/login');
    return false;
}

export {ifNotAuthenticated, ifAuthenticated};
